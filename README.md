# JSSpeccy_rastersoft: A ZX Spectrum emulator in pure JavaScript

## What is it?

This is a fork of JSSpeccy2, original from Matt Westcott.

JSSpeccy2 is a Sinclair Spectrum emulator written in Javascript,
that runs inside a browser.

Changes from the original JSSpeccy2:

* Emulates the floating bus
* The cursors emulate a kempston joystick
* Now sets the emulated canvas to the optimum size
* Added resize support
* Allows to load files from the URL

Details about the original version are in README.old

## Building

Perl, CoffeeScript[1] and Closure Compiler[2] are required. To build,
download *closure-compiler-vXXXXXXXX.jar*, move it to this folder, rename
it to *compiler.jar*, and type:

    make

This will place all web-distributable files into the 'dist' folder.

[1] http://jashkenas.github.com/coffee-script/  
[2] https://developers.google.com/closure/compiler/

## Browser support

Tested successfully on Chrome 32, Firefox 26 and Safari 7.0.1. Support for typed arrays and
the PixelData API are absolutely required, and it'll almost certainly stay that
way.

## Author (fork)

Sergio Costas Rodriguez  
rastersoft@gmail.com  
https://www.rastersoft.com  
